package ru.pisarev.tm.service;

import ru.pisarev.tm.api.IProjectTaskService;
import ru.pisarev.tm.api.ITaskRepository;
import ru.pisarev.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final ITaskRepository taskRepository;


    public ProjectTaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findTaskByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllTaskByProjectId(projectId);
    }

    @Override
    public Task bindTaskById(String taskId, String projectId) {
        if (taskId == null || taskId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.bindTaskToProjectById(taskId, projectId);
    }

    @Override
    public Task unbindTaskById(String taskId) {
        if (taskId == null || taskId.isEmpty()) return null;
        return taskRepository.unbindTaskById(taskId);
    }

    @Override
    public void removeAllTaskByProjectId(String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeAllTaskByProjectId(projectId);
    }

}