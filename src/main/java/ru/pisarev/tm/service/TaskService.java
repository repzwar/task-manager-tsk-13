package ru.pisarev.tm.service;

import ru.pisarev.tm.api.ITaskRepository;
import ru.pisarev.tm.api.ITaskService;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.findById(id);
    }

    @Override
    public Task findByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findByName(name);
    }

    @Override
    public void add(final Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null) return;
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.removeByName(name);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findByIndex(index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Task task = taskRepository.findByIndex(index);
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findByName(name);
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.findById(id);
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Task task = taskRepository.findByIndex(index);
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Task task = taskRepository.findByName(name);
        task.setStatus(Status.COMPLETED);
        return task;
    }

}
