package ru.pisarev.tm.api;

import ru.pisarev.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAllTaskByProjectId(String projectId);

    void removeAllTaskByProjectId(String projectId);

    Task bindTaskToProjectById(String taskId, String projectId);

    Task unbindTaskById(String id);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(int index);

    void add(Task task);

    void remove(Task task);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(int index);

    void clear();

}
