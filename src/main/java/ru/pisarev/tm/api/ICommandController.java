package ru.pisarev.tm.api;

public interface ICommandController {

    void displayHelp();

    void displayCommands();

    void displayArguments();

    void displayWait();

    void displayVersion();

    void displayAbout();

    void displayInfo();

    void exit();

}
