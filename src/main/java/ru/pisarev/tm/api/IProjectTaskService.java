package ru.pisarev.tm.api;

import ru.pisarev.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String projectId);

    Task bindTaskById(String taskId, String projectId);

    Task unbindTaskById(String taskId);

    void removeAllTaskByProjectId(String projectId);

}
